"""
Created on Mon May 13 13:04:34 2019

https://www.tensorflow.org/tutorials/keras/basic_classification
"""

from __future__ import absolute_import, division, print_function

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

# Helper libraries
import numpy as np
import matplotlib.pyplot as plt

print(tf.__version__)


def plot_image(i, predictions_array, true_label, img):
    predictions_array, true_label, img = predictions_array[i], true_label[i], img[i]
    plt.grid(False)
    plt.xticks([])
    plt.yticks([])

    plt.imshow(img, cmap=plt.cm.binary)

    predicted_label = np.argmax(predictions_array)
    if predicted_label == true_label:
        color = 'blue'
    else:
        color = 'red'

    plt.xlabel("{} {:2.0f}% ({})".format(class_names[predicted_label],
                                         100 * np.max(predictions_array),
                                         class_names[true_label]),
               color=color)


def plot_value_array(i, predictions_array, true_label):
    predictions_array, true_label = predictions_array[i], true_label[i]
    plt.grid(False)
    plt.xticks([])
    plt.yticks([])
    thisplot = plt.bar(range(10), predictions_array, color="#777777")
    plt.ylim([0, 1])
    predicted_label = np.argmax(predictions_array)

    thisplot[predicted_label].set_color('red')
    thisplot[true_label].set_color('blue')


def load_fashion_minst():
    # load MINST fasion dataset
    fashion_mnist = keras.datasets.fashion_mnist
    (train_images, train_labels), (test_images,
                                   test_labels) = fashion_mnist.load_data()
    # store class names of objects in images
    class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
                   'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

    return class_names, (train_images, train_labels), (test_images,
                                                       test_labels)


def look_at_nth_image(nth, predictions, test_labels, test_images):
    # look at the nth image
    i = nth
    plt.figure(figsize=(6, 3))
    plt.subplot(1, 2, 1)
    plot_image(i, predictions, test_labels, test_images)
    plt.subplot(1, 2, 2)
    plot_value_array(i, predictions, test_labels)
    plt.show()


def check_and_prep_image_data(class_names, train_images, train_labels, test_images, test_labels):

    # check data
    print("shape of training image data: ", train_images.shape)
    print("length of training label data: ", len(train_labels))
    print("label data: ", train_labels)
    print("shape of test image data: ", test_images.shape)
    print("length of test label data: ", len(test_labels))

    # view image from set
    plt.figure()
    plt.imshow(train_images[0])
    plt.colorbar()
    plt.grid(False)
    plt.show()

    # preprocess the data
    train_images = train_images / 255.0
    test_images = test_images / 255.0

    # veiw first 25 images
    plt.figure(figsize=(10, 10))
    for i in range(25):
        plt.subplot(5, 5, i + 1)
        plt.xticks([])
        plt.yticks([])
        plt.grid(False)
        plt.imshow(train_images[i], cmap=plt.cm.binary)
        plt.xlabel(class_names[train_labels[i]])
    plt.show()

    return class_names, (train_images, train_labels), (test_images,
                                                       test_labels)


def build_model(class_names, train_images, train_labels):
    # Build model
    input_shape = train_images[0].shape
    output_shape = len(class_names)

    # set up layers
    model = keras.Sequential([
        keras.layers.Flatten(input_shape=input_shape),
        keras.layers.Dense(128, activation=tf.nn.relu),
        keras.layers.Dense(output_shape, activation=tf.nn.softmax)
    ])

    # compile model
    model.compile(optimizer='adam',
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])

    # train model
    model.fit(train_images, train_labels, epochs=10)

    return model


def evaluate_accuracy(model, test_images, test_labels):
    # evaluate accuracy
    test_loss, test_acc = model.evaluate(test_images, test_labels)

    print('Test accuracy: ', test_acc)

    # make predictions on tests
    predictions = model.predict(test_images)
    print("prediction vector: ", predictions[0])
    print("top prediction: ", np.argmax(predictions[0]))
    print("test label: ", test_labels[0])

    # look at the 0th image
    look_at_nth_image(0, predictions, test_labels, test_images)

    # look at the 12th image
    look_at_nth_image(12, predictions, test_labels, test_images)
    # Plot the first X test images, their predicted label, and the true label
    # Color correct predictions in blue, incorrect predictions in red
    num_rows = 5
    num_cols = 3
    num_images = num_rows * num_cols
    plt.figure(figsize=(2 * 2 * num_cols, 2 * num_rows))
    for i in range(num_images):
        plt.subplot(num_rows, 2 * num_cols, 2 * i + 1)
        plot_image(i, predictions, test_labels, test_images)
        plt.subplot(num_rows, 2 * num_cols, 2 * i + 2)
        plot_value_array(i, predictions, test_labels)
    plt.show()


def make_single_prediction(model, class_names, test_images, test_labels):
    # Finally, use the trained model to make a prediction about a single image.
    # Grab an image from the test dataset
    img = test_images[0]
    print("image shape: ", img.shape)

    # Add the image to a batch where it's the only member.
    img = (np.expand_dims(img, 0))
    print("image shape: ", (img.shape))

    predictions_single = model.predict(img)
    print("prediciton for single image: ", predictions_single)

    # plot prediction
    plot_value_array(0, predictions_single, test_labels)
    plt.xticks(range(10), class_names, rotation=45)

    print("model prediction: ", np.argmax(predictions_single[0]))


if __name__ == "__main__":

    # load MINST fasion datase
    class_names, (train_images, train_labels), (test_images,
                                                test_labels) = load_fashion_minst()

    # check and prepare MINST image data
    class_names, (train_images, train_labels), (test_images, test_labels) = check_and_prep_image_data(
        class_names, train_images, train_labels, test_images, test_labels)

    # build the model
    model = build_model(class_names, train_images, train_labels)

    # evaluate the model
    evaluate_accuracy(model, test_images, test_labels)

    # use model to make a single prediction
    make_single_prediction(model, class_names, test_images, test_labels)


# @title MIT License
#
# Copyright (c) 2017 François Chollet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

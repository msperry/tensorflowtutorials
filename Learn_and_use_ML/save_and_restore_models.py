"""
Created on Mon May 13 16:25:06 2019

https://www.tensorflow.org/tutorials/keras/save_and_restore_models
"""

# Get an example dataset

from __future__ import absolute_import, division, print_function

import os

import tensorflow as tf
from tensorflow import keras

print(tf.__version__)


def create_model():
    model = tf.keras.models.Sequential([
        keras.layers.Dense(512, activation=tf.keras.activations.relu, input_shape=(784,)),
        keras.layers.Dropout(0.2),
        keras.layers.Dense(10, activation=tf.keras.activations.softmax)
    ])

    model.compile(optimizer=tf.keras.optimizers.Adam(),
                  loss=tf.keras.losses.sparse_categorical_crossentropy,
                  metrics=['accuracy'])

    return model


if __name__ == "__main__":

    (train_images, train_labels), (test_images,
                                   test_labels) = tf.keras.datasets.mnist.load_data()

    train_labels = train_labels[:1000]
    test_labels = test_labels[:1000]

    train_images = train_images[:1000].reshape(-1, 28 * 28) / 255.0
    test_images = test_images[:1000].reshape(-1, 28 * 28) / 255.0

    # Define a model

    # Returns a short sequential model

    # Create a basic model instance
    model = create_model()
    model.summary()

    # Save checkpoints during training
    # Checkpoint callback usage

    checkpoint_path = "training_1/cp.ckpt"
    checkpoint_dir = os.path.dirname(checkpoint_path)

    # Create checkpoint callback
    cp_callback = tf.keras.callbacks.ModelCheckpoint(checkpoint_path,
                                                     save_weights_only=True,
                                                     verbose=1)

    model = create_model()

    model.fit(train_images, train_labels, epochs=10,
              validation_data=(test_images, test_labels),
              callbacks=[cp_callback])  # pass callback to training

    # This may generate warnings related to saving the state of the optimizer.
    # These warnings (and similar warnings throughout this notebook)
    # are in place to discourage outdated usage, and can be ignored.

    # !dir {checkpoint_dir}
    print(os.listdir(checkpoint_dir))

    # Create a new, untrained model
    model = create_model()

    loss, acc = model.evaluate(test_images, test_labels)
    print("Untrained model, accuracy: {:5.2f}%".format(100 * acc))

    # load weights from rechpoint and reevaluate
    model.load_weights(checkpoint_path)
    loss, acc = model.evaluate(test_images, test_labels)
    print("Restored model, accuracy: {:5.2f}%".format(100 * acc))

    # Checkpoint callback options
    # include the epoch in the file name. (uses `str.format`)
    checkpoint_path = "training_2/cp-{epoch:04d}.ckpt"
    checkpoint_dir = os.path.dirname(checkpoint_path)

    cp_callback = tf.keras.callbacks.ModelCheckpoint(
        checkpoint_path, verbose=1, save_weights_only=True,
        # Save weights, every 5-epochs.
        period=5)

    model = create_model()
    model.save_weights(checkpoint_path.format(epoch=0))
    model.fit(train_images, train_labels,
              epochs=50, callbacks=[cp_callback],
              validation_data=(test_images, test_labels),
              verbose=0)

    # !dir {checkpoint_dir}
    print(os.listdir(checkpoint_dir))

    latest = tf.train.latest_checkpoint(checkpoint_dir)
    print(latest)

    # reset the model and load latest checkpoint to test
    model = create_model()
    model.load_weights(latest)
    loss, acc = model.evaluate(test_images, test_labels)
    print("Restored model, accuracy: {:5.2f}%".format(100 * acc))

    # Manually save weights
    # Save the weights
    model.save_weights('./checkpoints/my_checkpoint')

    # Restore the weights
    model = create_model()
    model.load_weights('./checkpoints/my_checkpoint')

    loss, acc = model.evaluate(test_images, test_labels)
    print("Restored model, accuracy: {:5.2f}%".format(100 * acc))

    # save the entire model

    # as HDF5 file
    model = create_model()

    model.fit(train_images, train_labels, epochs=5)

    # Save entire model to a HDF5 file
    model.save('my_model.h5')

    # and recreate model from file
    # Recreate the exact same model, including weights and optimizer.
    new_model = keras.models.load_model('my_model.h5')
    new_model.summary()

    # Check its accuracy
    loss, acc = new_model.evaluate(test_images, test_labels)
    print("Restored model, accuracy: {:5.2f}%".format(100 * acc))

    # As a saved_model
    model = create_model()

    model.fit(train_images, train_labels, epochs=5)

    # Create a saved_model
    saved_model_path = tf.contrib.saved_model.save_keras_model(
        model, r".\saved_models")

    # !dir {"saved_models/"}
    print(os.listdir("saved_models/"))

    # Reload a fresh keras model from the saved mode
    new_model = tf.contrib.saved_model.load_keras_model(saved_model_path)
    new_model.summary()

    # run restored model
    # The model has to be compiled before evaluating.
    # This step is not required if the saved model is only being deployed.

    new_model.compile(optimizer=tf.keras.optimizers.Adam(),
                      loss=tf.keras.losses.sparse_categorical_crossentropy,
                      metrics=['accuracy'])

    # Evaluate the restored model.
    loss, acc = new_model.evaluate(test_images, test_labels)
    print("Restored model, accuracy: {:5.2f}%".format(100 * acc))


# @title MIT License
#
# Copyright (c) 2017 François Chollet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

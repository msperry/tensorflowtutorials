"""
Created on Mon May 13 13:04:34 2019

https://www.tensorflow.org/tutorials/keras/basic_text_classification
"""


from __future__ import absolute_import, division, print_function

import tensorflow as tf
from tensorflow import keras

import matplotlib.pyplot as plt


print(tf.__version__)


def decode_review(text, reverse_word_index):
    return ' '.join([reverse_word_index.get(i, '?') for i in text])


def load_imdb_data():

    # download imdb dataset
    imdb = keras.datasets.imdb

    (train_data, train_labels), (test_data,
                                 test_labels) = imdb.load_data(num_words=10000)

    # look at data size
    print(
        "Training entries: {}, labels: {}".format(
            len(train_data),
            len(train_labels)))
    print(train_data[0])
    print(len(train_data[0]), len(train_data[1]))

    return imdb, (train_data, train_labels), (test_data, test_labels)


def convert_imdb_integers2words(imdb):
    # A dictionary mapping words to an integer index
    word_index = imdb.get_word_index()

    # The first indices are reserved
    word_index = {k: (v + 3) for k, v in word_index.items()}
    word_index["<PAD>"] = 0
    word_index["<START>"] = 1
    word_index["<UNK>"] = 2  # unknown
    word_index["<UNUSED>"] = 3

    reverse_word_index = dict([(value, key)
                               for (key, value) in word_index.items()])

    print(decode_review(train_data[0], reverse_word_index))

    return word_index


def pad_data(train_data, test_data, word_index):
    # prepare the data
    train_data = keras.preprocessing.sequence.pad_sequences(
        train_data, value=word_index["<PAD>"], padding='post', maxlen=256)

    test_data = keras.preprocessing.sequence.pad_sequences(
        test_data, value=word_index["<PAD>"], padding='post', maxlen=256)

    print("\nprepred data length", (len(train_data[0]), len(train_data[1])))
    print(train_data[0])

    return train_data, test_data


def build_model(train_data, train_labels):
    # input shape is the vocabulary count used for the movie reviews (10,000
    # words)
    vocab_size = 10000  # find out where this value comes from

    model = keras.Sequential()
    model.add(keras.layers.Embedding(vocab_size, 16))
    model.add(keras.layers.GlobalAveragePooling1D())
    model.add(keras.layers.Dense(16, activation=tf.nn.relu))
    model.add(keras.layers.Dense(1, activation=tf.nn.sigmoid))

    model.summary()

    # configure the model to use an optimizer and a loss function
    model.compile(optimizer='adam',
                  loss='binary_crossentropy',
                  metrics=['acc'])

    # create validataion data set
    x_val = train_data[:10000]
    partial_x_train = train_data[10000:]

    y_val = train_labels[:10000]
    partial_y_train = train_labels[10000:]

    # train the model
    history = model.fit(partial_x_train,
                        partial_y_train,
                        epochs=40,
                        batch_size=512,
                        validation_data=(x_val, y_val),
                        verbose=1)

    return model, history


def plot_training_and_validation(epochs, train_value, val_value, acc_or_loss):

    # "bo" is for "blue dot"
    plt.plot(epochs, train_value, 'bo', label='Training ' + acc_or_loss)
    # b is for "solid blue line"
    plt.plot(epochs, val_value, 'b', label='Validation ' + acc_or_loss)
    plt.title('Training and validation ' + acc_or_loss)
    plt.xlabel('Epochs')
    plt.ylabel(acc_or_loss.capitalize())
    plt.legend()

    plt.show()


def plot_history_over_time(history):
    # create a graph of accuracy and loss over time
    history_dict = history.history
    print(history_dict.keys())

    acc = history_dict['acc']
    val_acc = history_dict['val_acc']
    loss = history_dict['loss']
    val_loss = history_dict['val_loss']

    epochs = range(1, len(acc) + 1)

    plot_training_and_validation(epochs, loss, val_loss, 'loss')

    plt.clf()   # clear figure

    plot_training_and_validation(epochs, acc, val_acc, 'accuracy')


def evaluate_results(model, test_data, test_labels, history):
    # evaluate the model
    results = model.evaluate(test_data, test_labels)

    print(results)

    # create a graph of accuracy and loss over time
    plot_history_over_time(history)


if __name__ == "__main__":

    # download imdb dataset
    imdb, (train_data, train_labels), (test_data,
                                       test_labels) = load_imdb_data()

    # convert integers back to words
    word_index = convert_imdb_integers2words(imdb)

    # prepare the data
    train_data, test_data = pad_data(train_data, test_data, word_index)

    # build the model
    model, history = build_model(train_data, train_labels)

    # evaluate the model
    evaluate_results(model, test_data, test_labels, history)


# @title MIT License
#
# Copyright (c) 2017 François Chollet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

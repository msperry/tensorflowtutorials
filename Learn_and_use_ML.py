"""
Created on Tue May 14 16:17:30 2019

https://www.tensorflow.org/tutorials/keras
"""

import learn_and_use_ML.basic_classification as bascla
import learn_and_use_ML.text_classification as txtcal
import learn_and_use_ML.basic_regression as basreg
import learn_and_use_ML.overfit_and_underfit as oftuft
import learn_and_use_ML.save_and_restore_models as sarmod


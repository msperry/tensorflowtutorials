# TensorFlowTutorials

Tensorflow guide and tutorials:

Goals:
1. basic use and understanding
2. refactoring into reusable chunks for other projects

Source:
* https://www.tensorflow.org/overview
* https://www.tensorflow.org/tutorials
* https://www.tensorflow.org/guide
